import React, { Component } from "react";

class Display extends Component {

    constructor(props) {
        super(props)

        this.state = {
            ms: 0,
            s: 0,
            m: 0,
            h: 0
        }
    }

    stop() {
        clearInterval(this.runner);
    }

    reset () {
        this.setState({
            ms: 0,
            s: 0,
            m: 0,
            h: 0
        })
    }

    start() {
        clearInterval(this.runner)
        this.runner = setInterval(()=>{
            this.setState({
                ms: this.state.ms += 1
            });
            if (this.state.ms >= 99) {
                this.setState({
                    ms: 0,
                    s: this.state.s += 1
                })
            }
            if (this.state.s >= 59) {
                this.setState({
                    s: 0,
                    m: this.state.m += 1
                })
            }
            if (this.state.m >= 59) {
                this.setState({
                    m: 0,
                    h: this.state.h += 1
                })
            }
        })
    }
    render() {
        return (
            <div>
                <div>
                    <h1>{this.state.h}:{this.state.m}:{this.state.s}:{this.state.ms}</h1>
                </div>
                <button onClick={() => this.start()}>Start</button>
                <button onClick={() => this.stop()}>Pause</button>
                <button onClick={() => this.reset()}>Reset</button>
            </div>
        )
    }
}

export default Display;